<?php
//Главная страница
require_once("core/initialize.php");
$pc=new PostController($conn);

?>
<!DOCTYPE html>
<html lang="en"  >
  <head>
  <?php Page::part('head'); ?>
 </head>
<body >
  <?php Page::part('navbar'); ?>
  <div class="d-flex flex-wrap">  
  <?php
  //получаем все объявления из базы и показываем на стене 
     $arr=$pc->get_posts();
      foreach($pc->get_posts() as $post){
        //если объявление неактуально пропускаем идем дальше
          if(!$post["relevance"]) continue;
          //если объявление без фото, устанавливаем фото поумолчанию
          $path_img=$post['img']?$post['img']:"NOT_IMAGE.jpg";
     ?>
        <div class="card mb-4 rounded-3 shadow-sm">
              <div class="card-header py-3">
                <h4 class="my-0 fw-normal"><?=$post['name']?></h4>
              </div>
              <div class="card-body" style="display:flex; flex-direction: column; align-items: center;">
                <h1 class="card-title pricing-card-title"><?=$post['price']?>$</h1>
                <ul class="list-unstyled mt-3 mb-4" style="height: 60%;">
                  <li><?=$post['address']?></li>
                  <li><img class="img-thumbnail" src="assets/images/<?=$path_img?>" /></li>
                  <li><?=$post['description']?></li>  
                </ul>
                <button  class="w-100 btn btn-lg btn-outline-primary">Подробнее</button>
          
                <?php if(isset($_SESSION["user"]['role']) && $_SESSION["user"]["role"]==="admin"):?>
                
                <form action="<?="web/update.php"?>" method="GET" style="width:100%">
                <button class="w-100 btn btn-lg btn-outline-primary"> Редактировать </button>
                <input hidden name="id" value="<?=$post['id']?>">
                </form>
                <?php endif;?>
  
              </div>
      </div>
      <?php } ?>
  </div>
  <?php Page::part('footer'); ?>
</body>
</html>