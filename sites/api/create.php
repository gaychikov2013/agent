<?php
/*Скрипт по созданию новой записи в базе данных. Мы используем заголовки для нашего 
клиента(браузера).
*/
header("Content-Type: application/json; charset=UTF-8"); /*Отправка данных в json формате */
header("Access-Control-Allow-Methods: POST");/*Говорим браузеру что доступ к ресурсу разрешен только через метод POST*/
require_once("../core/initialize.php");
$data = $_POST;/* Глобальная переменная POST хранит информацию переданную от клиента для сохранения в бд */
$data["img"]=$_FILES["img"]["name"];
$pc= new PostController ($conn,$data); /* создаем контроллер, чтобы вызвать фунцию добавления данных в бд */
$r=move_uploaded_file($_FILES["img"]["tmp_name"],"../assets/images/".$data["img"]); /* фукнцию отправляет фото в папку images на сохранения */
if(!$r)
{   
    http_response_code(500);
    echo json_encode(['status'=>false, /*Если фото не сохранилась говорим это кодом 500 */
    'message'=>'Image not saved']);  
}
elseif($pc->create())
{        http_response_code(201);
    echo json_encode(['status'=>true, /*Если все отлично говорим это кодом 201 */
    'message'=>"Post created"]);  
}
else
{
    http_response_code(500);
    unlink("../assets/images/".$data["img"]);
    echo json_encode([
        "status"=> false,   /*Если все плохо говорим это кодом 500 */
        "message"=>"Error"
    ]);
}     
?>