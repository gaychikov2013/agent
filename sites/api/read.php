<?php
 header("Content-Type: text/html; charset=UTF-8");
$pc= new PostController ($conn);
$data=[];
if(isset($_GET['id']))
  $r=$pc->get_post($_GET['id']);
else $r=$pc->get_posts();
if($r) 
{   http_response_code(200);
    echo json_encode($r); 
}
else{
    http_response_code(404);
    echo json_encode([
            "status"=>(bool)$r,
            "message"=>'Post not found'
        ]);  
}
?>