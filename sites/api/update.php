<?php
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
require_once("../core/initialize.php");
$data =$_POST;
$data["img"]=$_FILES["img"]["name"];
$pc= new PostController ($conn,$data);
if (!$pc->update())
{
    http_response_code(400);
    return json_encode(
        ['status'=>false,
        'message'=>'Post not updated']);
}
else{
    http_response_code(200);
    return json_encode(
        ['status'=>true,
        'message'=>"Post updated"]);
}

?>