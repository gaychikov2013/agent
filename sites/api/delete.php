<?php
header("Content-Type: application/json; charset=UTF-8");
$pc= new PostController ($conn);
if(isset($_GET['id']))
     if($pc->delete($_GET['id']))
     {
        http_response_code(200);
        return  json_encode([
            'status'=>true,
            "message"=>"Post deleted"]);
     }
     else
     {
        http_response_code(400);
        $response;
        return json_encode([
            'status'=>false,
            "message"=>'Post not deleted']);
     }
?>