<?php

//создание класса с основными свойствами объектам недвижимости 
class Post
{
    public $id;
    public $name;
    public $address;
    public $description;
    public $price;
    public $relevance;// актуальность
    public $img; 
}

class PostController
{
    private $table='posts';
    private $conn;
    private $post;
    public function __construct($db,$data=[])
    {
        //передается в конструктор объект подключения к базе данных и масссив данных для передачи 
        //на добавление в базу или обновления в базе, по умолчанию пустой массив
        $this->conn=$db;
        $this->post=new Post();
        //создаем объект post и инициализируем его поля, если массив data пустой цикл не запускается
        foreach($data as $key=>$prop)
            $this->post->$key=$prop;
    }
    // получаем запись из бд по id
    public function get_post($id)
    {
        $query="SELECT * FROM ".$this->table." WHERE id=:id";
        $stm=$this->conn->prepare($query);
        $stm->execute([$id]);
        $data=$stm->fetch();
        return  $data;
    }
    // получаем все записи из бд
    public function get_posts()
    {
        $result=$this->conn->query("SELECT * FROM ".$this->table); 
        $data=array(); 
        while($row=$result->fetch())
        {
            $data[]=$row;        
        }
        return $data;
    }
    //делаем запрос на удаление записи по id
    public function delete($id)
    {   /*Поиск названия файла для удаления */
        $query_img="SELECT img FROM ".$this->table." WHERE id=:id"; // запрос SQL к базе для удаления записи
        $stm_query_img=$this->conn->prepare($query_img);
        $result=$stm_query_img->execute([$id]);
        //создаем транзакцию 
        /*Транзакции SQL – это группа последовательных операций с базой данных, 
        которая представляет собой логическую единицу работы с данными. Иными словами, 
        транзакции позволяют нам контролировать процессы сохранения и изменения в базах данных. 
        Если в процесе удаления, добавления, обновления произошла ошибка все действия выполненные откатываются назад*/
        $this->conn->beginTransaction();
        $query="DELETE FROM ".$this->table." WHERE id=:id";
        $stm=$this->conn->prepare($query);
        try{
        $result=$stm->execute([$id]);
        $this->conn->commit(); //если все хорошо завершаем транзакцию
        unlink("assets/images/".$stm_query_img->fetch()['img']);  //удаляем фото из папки
        return $result;  //возращаеся true
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage();     
            $this->conn->rollBack();//если все плохо откатываемся назад
      
        }
        return false;
    }
//обновление записи в бд
    public function update()
    {
        $u_post=$this->post;
        $this->conn->beginTransaction();
        $query="UPDATE ".$this->table." SET name=:name,address=:address,description=:description,price=:price,relevance=:relevance,img=:img WHERE id=:id";
        $stm=$this->conn->prepare($query);
        try{
            $result=$stm->execute([$u_post->name, $u_post->address,$u_post->description,$u_post->price,$u_post->relevance,$u_post->img,$u_post->id]);
            $this->conn->commit();
            return $result;
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage();
            $this->conn->rollBack();
        }
       return false;  
    }

    //создание новой записи, снова используем транзакцию
    public function create()
    {
        $n_post=$this->post;
        $this->conn->beginTransaction();
        $query="INSERT INTO ".$this->table." SET name=:name,address=:address,description=:description,price=:price,relevance=:relevance,img=:img";
        $stm=$this->conn->prepare($query);
        try{
            $result=$stm->execute([ $n_post->name, $n_post->address,$n_post->description,$n_post->price,$n_post->relevance,$n_post->img]);
            $this->conn->commit();
            return $result;   
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage();
            $this->conn->rollBack();
        }
       
           return false;
    }  
}
?>