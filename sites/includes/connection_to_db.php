<?php 
$db_user='root';
$db_password='';
$db_name='agent';

/*Создание PDO объекта, создание строки подключения и указание необходимых констант в массиве*/
$opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false
    ];
$conn=new PDO('mysql:host=127.0.0.1;dbname='.$db_name.';charset=utf8', $db_user, $db_password,$opt);
?>