

<?php 
// Тоже самое что и с post, только здесь пользователь, функций меньше, 
//так как обновление данных пользователя и удаление не сделали
class User
{
    public $first_name;
    public $last_name;
    public $login;
    public $password;
    public $email;
    public $role;
}

class UserController
{
    private $table='users';
    private $conn;
    public function __construct($db)
    {
        $this->conn=$db;
    }
    public function get_user($email,$password)
    {
        $query="SELECT * FROM ".$this->table." WHERE email=:email AND password=:password";
        $stm=$this->conn->prepare($query);
        $stm->execute([$email,$password]);
        $data=$stm->fetch();
        return  $data;
    }
   public function create($data)
   {
        $first_name=$data["first_name"];
        $last_name=$data["last_name"];
        $login=$data["login"];
        $password=md5($data["password"]);
        $email=$data["email"];
        $query="INSERT INTO ".$this->table." SET firstname=:first_name,lastname=:last_name,login=:login,password=:password,email=:email";
        $stm=$this->conn->prepare($query);
        $result=$stm->execute([$first_name,$last_name,$login,$password,$email]);
        return $result;
   }

}
?>