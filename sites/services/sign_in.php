<?php 
//При авторизации пользователь передает запрос вида POST со своим email и password
require_once("../core/initialize.php");
$uc= new UserController($conn);
$email=$_POST["email"];
$password=$_POST["password"];
$password=md5($password);//шифруем пароль, так как небезопасно держать его открытым
$user=$uc->get_user($email,$password); //по email и password получаем из базы запись о пользователе
if($user)//если такой существует, добавляем данные в сессию
{
      $_SESSION['user']=[
           "email"=>$user["email"],
           "login"=>$user["login"],
           "role"=>$user["role"]
      ];
      header('Location:/');
}
?>