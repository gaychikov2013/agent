<?php
//скрипт запускается когда происходи нажатие кнопки "Применить изменения", логика такая же как в api
require_once("../core/initialize.php");

$data =$_POST;
$data["img"]=$_FILES["img"]["name"];
$pc= new PostController($conn,$data);

$path="../assets/images";
$old_post=$pc->get_post($data['id']);

if(!in_array($data["img"],scandir($path)))
    {  
            unlink("../assets/images/".$old_post['img']);
            move_uploaded_file($_FILES["img"]["tmp_name"],$path.$data["img"]);
       
    }  
if (!$pc->update())
    {
        http_response_code(400);
        return json_encode(
            ['status'=>false,
            'message'=>'Post not updated']);
    }
else header('Location: /');

?>