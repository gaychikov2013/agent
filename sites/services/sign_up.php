<?php
//Регистрация пользователя, ничего нового
require_once("../core/initialize.php");//всегда подключаем скрипт загрузчик
$uc= new UserController($conn);
$data = $_POST;
if($uc->create($data))
{
    echo json_encode(['status'=>true,
    'message'=>"user created"]);  
     header("Location: /");
}
else
{
    //если ошибка, отправляем в виде json, так как страницу для ошибки не создали
    header("Content-Type: application/json; charset=UTF-8");
    echo json_encode([
        "status"=> false,
        "message"=>"Error"
    ]);
}     
?>