<?php
//скрипт запускается когда происходи нажатие кнопки "Удалить", логика такая же как в api
require_once("../core/initialize.php");
$pc= new PostController ($conn);
if(isset($_GET['id']))
     if($pc->delete($_GET['id']))
     {
       header('Location:/');
     }
     else
     {
        http_response_code(400);
        return json_encode([
            'status'=>false,
            "message"=>'Post not deleted']);
     }
?>
