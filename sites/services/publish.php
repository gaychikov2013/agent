<?php
//скрипт запускается когда происходит нажатие кнопки "Опубликовать", логика такая же как в api
require_once("../core/initialize.php");
$data = $_POST;
// Мы передаем файл веб серверу, данные о файле хранятся в переменной FILES
$data["img"]=$_FILES["img"]["name"]; //Получаем имя файла и сохранем в массив data по ключу img
$pc= new PostController ($conn,$data );
$r=move_uploaded_file($_FILES["img"]["tmp_name"],"../assets/images/".$data["img"]);
if(!$r)
{   
    http_response_code(500);
    echo json_encode(['status'=>false,
    'message'=>'Image not saved']);  
}
if($pc->create())
   header("Location: /");
else
{
    unlink("../assets/images/".$data["img"]);
    echo json_encode([
        "status"=> false,
        "message"=>"Error"
    ]);
}     
?>