<?php require_once("../core/initialize.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php Page::part('head'); ?>
 </head>
<body>
<?php Page::part('navbar'); ?>
    <form action="<?=DS."services".DS."publish.php"?>" method="POST" enctype="multipart/form-data">
    <div class="container mt-5">
        <div class="row col-12"> 
        <div class="form-group">
            <label> Заголовок объявления</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label>Адрес </label>
            <input type="text" class="form-control"  name="address">
        </div>
        <div class="form-group">
            <label > Описание</label>
            <textarea class="form-control"  name="description"> </textarea>
        </div>
        <div class="form-group">
            <label >Цена</label>
            <input type="text" class="form-control"  name="price">
        </div>
        <div class="form-group">
            <label> Актуальность</label>
            <input type="checkbox" name="relevance">
        </div>
        <div class="form-group">
            <label>Изображение</label>
            <input type="file" class="form-control"  name="img">
        </div>
        </div>
        <div class="form-group">
           <button class="btn btn-primary" id="btn_public" >Опубликовать </button>
        </div>
</div>
</div>
</form>
<?php Page::part('footer'); ?>
</body>
</html>