<?php 
//Страница авторизации
require_once("../core/initialize.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>

<?php 
//С помощью класса Page подключаем компоненты 
Page::part('head'); ?>
 </head> 
<body>
<?php
//Вместо блока header теперь navbar
     Page::part('navbar'); ?>
    <form class="form-auth" action="<?=DS."services".DS."sign_in.php"?>" method="POST">
        <h1 class="h3 mb-3 fw-normal">Укажите данные для входа</h1>
        <div class="form-floating">
          <input type="email" class="form-control" name="email" placeholder="name@example.com">
          <label>Email</label>
        </div>
        <div class="form-floating">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <label >Пароль</label>
        </div>
        <button class="btn btn-primary w-100 py-2" type="submit">Войти</button>
      </form>
      <?php Page::part('footer'); ?>
</body>
</html>

