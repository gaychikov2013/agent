<?php require_once("../core/initialize.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php Page::part('head'); ?>
 </head>
<body>
  <?php Page::part('navbar'); 
   $pc=new PostController($conn);
   $post=$pc->get_post($_GET['id']);

?>
    <form action="<?="/services/change_post.php"?>" method="POST" enctype="multipart/form-data">
    <input name="id" hidden  value="<?=$post["id"]?>">
    <div class="container mt-5">
        <div class="row col-12"> 
        <div class="form-group">
            <label> Заголовок объявления</label>
            <input type="text" class="form-control" name="name" value="<?=$post["name"]?>">
        </div>
        <div class="form-group">
            <label>Адрес </label>
            <input type="text" class="form-control"  name="address"  value="<?=$post["address"]?>">
        </div>
        <div class="form-group">
            <label > Описание</label>
            <textarea class="form-control" name="description"><?=$post["description"]?> </textarea>
        </div>
        <div class="form-group">
            <label >Цена</label>
            <input type="text" class="form-control"  name="price"  value="<?=$post["price"]?>">
        </div>
        <div class="form-group">
            <input type="file" class="form-control"  name="img"  value="<?=$post["img"]?>">
        </div>
        <div class="form-group">
            <label> Актуальность</label>
            <input type="checkbox" name="relevance" <?=$post["relevance"]?"checked":"" ?> value="<?=$post["relevance"]?>">
        </div>
        <br>
        <br>
        </div>
        <div class="form-group">
           <button class="btn btn-primary" id="btn_change" >Применить изменения </button>
        </div>
</div>
</div>
</form>
<form action="<?="/services/delete_publish.php"?>">
<div class="container mt-2">
    <div class="form-group">
    <input name="id" hidden  value="<?=$post["id"]?>">
    <button class="btn btn-danger">Удалить</button>
    </div>
</div>
</form>


<?php Page::part('footer'); ?>
</body>
</html>