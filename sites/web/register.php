<?php require_once("../core/initialize.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php Page::part('head'); ?>
 </head>
<body>
<?php Page::part('navbar'); ?>
<div class="col-md-7 col-lg-8 form-reg">
        <h4 class="mb-3">Регистрация пользователя</h4>
        <form class="needs-validation " novalidate="" action="<?=DS."services".DS."sign_up.php"?>" method="POST">
          <div class="row g-3">
            <div class="col-sm-6">
              <label class="form-label">Имя</label>
              <input type="text" class="form-control" name="first_name" placeholder="" value="" required="">
              <div class="invalid-feedback">
                Valid first name is required.
              </div>
            </div>
            <div class="col-sm-6">
              <label  class="form-label">Фамилия</label>
              <input type="text" class="form-control" name="last_name" placeholder="" value="" required="">
              <div class="invalid-feedback">
                Valid last name is required.
              </div>
            </div>
            <div class="col-12">
              <label  class="form-label">Логин</label>
              <div class="input-group has-validation">
                <input type="text" class="form-control" name="login" placeholder="Username" required="">
              <div class="invalid-feedback">
                  Your username is required.
                </div>
              </div>
            </div>
            <div class="col-12">
                <label  class="form-label">Пароль</label>
                <div class="input-group has-validation">
                  <input type="text" class="form-control" name="password" placeholder="Password" required="">
                <div class="invalid-feedback">
                    Your password is required.
                  </div>
                </div>
              </div>
            <div class="col-12">
              <label  class="form-label">Email <span class="text-body-secondary">(Optional)</span></label>
              <input type="email" class="form-control" name="email" placeholder="you@example.com">
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>
          <button class="w-100 btn btn-primary btn-lg" type="submit" id="btn_reg">Регистрация</button>
        </form>
      </div>
      
      <?php Page::part('footer'); ?>

</body>
</html>




