<?php $date=2023?>
<footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
      <div class="col-12 col-md">
        <img class="mb-2" src="/assets/images/logo.png" width="215" height="200">
        
      </div>
      <div class="col-6 col-md">
        <h5>Ресурсы</h5>
        <ul class="list-unstyled text-small">
          <li class="mb-1"><a class="link-secondary text-decoration-none" href="#">Новости</a></li>
        </ul>
      </div>
      <div class="col-6 col-md">
        <h5>О Нас</h5>
        <ul class="list-unstyled text-small">
          <li class="mb-1"><a class="link-secondary text-decoration-none" href="#">Команда</a></li>
          <li class="mb-1"><a class="link-secondary text-decoration-none" href="#">Адрес</a></li>
        </ul>
      </div>
      <h4 class="d-block mb-3" align="center"><?=$date ?></h4>
    </div>
  </footer>
</div>
